ebiexperts API Samples
######################

In this tutorial we will describe the ways to access WIP or SAM Rest API using Node.js

We provide also 2 samples that describe how to connect Qlik Sense on premise and SaaS Rest API

Installation
************

All products used below are free of charges

1) Install node.js from https://nodejs.org/en/ 
2) Not mandatory but you can Install Visual Studio Code for more convenience from https://code.visualstudio.com/
3) Install Git https://git-scm.com/downloads
4) Clone this repos fro bitbucket from https://ebiexpertsEM_VM/api.git
5) Open folder ".\api\Node JS\wip rest api sample" in Visual Studio Code 
6) Type npm install
7) Open wip_api01.js
8) Change the line "const furl = 'https://localhost:59272';" with your WIP url
9) Open wip environment and get the environment id in the url
10) change line "var   fbody = 'environment=661286&grant_type=password&login_method=0&password=EBIEXPERTS&plugin_type=-1&username=VMMANAGER';" with the environment id you have

