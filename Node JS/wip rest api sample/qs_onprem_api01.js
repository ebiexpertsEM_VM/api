/**
 * Qlik Sense On Premise Rest API Sample
 *
 * Ref: https://ebiexpert.atlassian.net/wiki/spaces/WIPW/pages/edit-v2/2132803678
 *
 * @install :  npm install
 * @run     :  node qs_onprem_api01.js
 */

import fetch from 'node-fetch';
import https from 'https';
import fs from 'fs';
import path from 'path';
import { fileURLToPath } from 'url';
import { dirname } from 'path';
const __filename = fileURLToPath(import.meta.url);
const __dirname = dirname(__filename);

console.clear();

// Avoid error on self signed certificates
process.env.NODE_TLS_REJECT_UNAUTHORIZED = 0;

// URL
const furl = 'https://demo.ebiexperts.online:4242';
const fxrf = '?xrfkey=abcdefghijklmnop';
// Set the Request header for further calls
var fheaders = {
  'Content-Type'  : 'application/json',
  'x-qlik-xrfkey' : 'abcdefghijklmnop',
  'X-Qlik-User'   : 'UserDirectory= Internal; UserId= sa_repository '
};
const pfx = fs.readFileSync(path.resolve(__dirname, './certificates/220/client.pfx'));
const fagent = https.Agent({pfx});

// Get list of apps
const fresponse = await fetch(furl + '/qrs/app' + fxrf, { method: 'GET', headers: fheaders , agent: fagent});
if (fresponse.status != 200){
  console.log('Error:' + fresponse.status + ' :: ' + fresponse.statusText);
} else {  
  const fdata = await fresponse.json();
  console.log(fdata);
}
