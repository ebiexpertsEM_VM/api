/**
 * WIP Rest API Sample
 *
 * Ref: https://ebiexpert.atlassian.net/wiki/spaces/WIPW/pages/edit-v2/2132803678
 *
 * @install :  npm install
 * @run     :  node wip_api01.js
 */

import fetch from 'node-fetch';
console.clear();

// Avoid error on self signed certificates
process.env.NODE_TLS_REJECT_UNAUTHORIZED = 0;
const furl = 'https://demo.ebiexperts.online:59272';

// Get the bearer token
var   fbody = 'environment=206&grant_type=password&login_method=0&password=EBIEXPERTS&plugin_type=-1&username=VMMANAGER';
const fresponse = await fetch(furl + '/token', { method: 'POST', body: fbody });
const fdata = await fresponse.json();
//console.log(data);

// Set the Request header for further calls
var fheaders = {
    Authorization: 'bearer ' + fdata.access_token
};

// Test get all actors
const fresponse2 = await fetch(furl + '/api/Actors/GetAllActorsNames', { method: 'GET', headers: fheaders });
const fdata2 = await fresponse2.json();
console.log(fdata2);

// Fetch all actors
fdata2.data.forEach(vrActor => {
  console.log(vrActor.name, vrActor.id);
});


