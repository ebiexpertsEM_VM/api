/**
 * Qlik Sense SaaS Rest API Sample
 *
 * Ref: https://ebiexpert.atlassian.net/wiki/spaces/WIPW/pages/edit-v2/2132803678
 *
 * @install :  npm install
 * @run     :  node wip_api01.js
 */

import fetch from 'node-fetch';
console.clear();

// Avoid error on self signed certificates
//process.env.NODE_TLS_REJECT_UNAUTHORIZED = 0;

// URL
const furl = 'https://ebiexperts.eu.qlikcloud.com';
const fapikey = 'eyJhbGciOiJFUzM4NCIsImtpZCI6Ijc2OGIzZDFlLWVlNjEtNDI2Ni1hODA0LTk1NWQzZTliNmFmZSIsInR5cCI6IkpXVCJ9.eyJzdWJUeXBlIjoidXNlciIsInRlbmFudElkIjoiTHdXLWQxbXQ1UVMzQUJ1QXdTRk9XeXROdTg0ZGt6Si0iLCJqdGkiOiI3NjhiM2QxZS1lZTYxLTQyNjYtYTgwNC05NTVkM2U5YjZhZmUiLCJhdWQiOiJxbGlrLmFwaSIsImlzcyI6InFsaWsuYXBpL2FwaS1rZXlzIiwic3ViIjoiSTREY3d0RVNYczJYVTM3VGthNmo1V3NwNzhBZHYzMjIifQ.qJxEvpRxfN8HxE775JvVaGlsydgkiD3Y5HbPcQLhDiM1h3G678qpH-u32pAnUTuHsd3kpaOYUUS5U1W8CS3engTWmah4eUmwI4J_R1-EJAg4b4f-59MFbVrjGYNUiqnD';
// Set the Request header for further calls
var fheaders = {
  'Content-Type': 'application/json',
  'Authorization': 'Bearer ' + fapikey
};

// Create a new app
//const fresponse = await fetch(furl + '/api/v1/apps', { method: 'POST', headers: fheaders });

// Get list of apps
const fresponse = await fetch(furl + '/api/v1/items', { method: 'GET', headers: fheaders });
if (fresponse.status != 200){
  console.log('Error:' + fresponse.statusText);
} else {  
  const fdata = await fresponse.json();
  console.log(fdata);
}
