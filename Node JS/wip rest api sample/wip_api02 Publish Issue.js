/**
 * WIP Rest API Sample
 * Get List of Pending issues in a Business Unit and Publish them
 *
 * Ref: https://ebiexpert.atlassian.net/wiki/spaces/WIPW/pages/edit-v2/2132803678
 *
 * @install :  npm install
 * @run     :  node wip_api01.js
 */

import fetch from 'node-fetch';
console.clear();

// Avoid error on self signed certificates
process.env.NODE_TLS_REJECT_UNAUTHORIZED = 0;

// Function parameters
const furl = 'https://10.0.1.107:59273'; 
const fbusinessUnitID = 49;
const vmmanagerUserID = 37;

// Get the bearer token
var fbody = 'environment=661286&grant_type=password&login_method=0&password=EBIEXPERTS&plugin_type=-1&username=VMMANAGER';
const fresponse = await fetch(furl + '/token', { method: 'POST', body: fbody });
const fdata = await fresponse.json();
//console.log(data);

// Set the Request header for further calls
var fheaders = {
  Authorization: 'bearer ' + fdata.access_token
};

// Filter issues as required for POST Methods
var fissueFilter = {
  "active": true,
  "full": true,
  "assignedToMe": true,
  "reportedByMe": true,
  "subscribedTo": true,
  "recentlyUpdated": true,
  "releaseType": 0,
  "businessUnitID": fbusinessUnitID,
  "includeBacklog": false,
  "isBacklogOnly": true
}

// Get pendingPublications for a Business Unit
const fresponse2 = await fetch(furl + '/api/Issues/GetUserIssues/'+vmmanagerUserID+'?businessUnitId=' + fbusinessUnitID, { method: 'GET', headers: fheaders });
if (fresponse2.status != 200) {
  console.log('Error:' + fresponse2.statusText);
} else {
  const fdata2 = await fresponse2.json();
  console.log(fdata2);

  // Fetch all pending publications
  fdata2.data.pendingPublications.publication.forEach(async function (publication) {
      console.log(publication.id, publication.publishBy);

      // Publish the pending publication
      const fresponse3 = await fetch(furl + '/api/WipApi/PublishTip?tipId=' + publication.id + '&reload=true', { method: 'GET', headers: fheaders });
      console.log(fresponse3.statusText);
      // if (fresponse3.status != 200) {
      //   debugger;
      // }
    });
}


